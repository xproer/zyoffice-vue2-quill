window.wangEditor.zyoffice = {
    init: function(editor,w) {    	
    	$(editor).find(" .w-e-toolbar").append("<div class='w-e-menu zyoffice' title='导入Word文档（docx格式）' onclick='window.wangEditor.zyoffice.run()'></div>");
    },
	run: function(){
        zyOffice.getInstance().api.openDoc();
	}
};
window.wangEditor.zywordexport = {
    init: function(editor,w) {    	
    	$(editor).find(" .w-e-toolbar").append("<div class='w-e-menu zywordexport' title='导出Word文档（docx格式）' onclick='window.wangEditor.zywordexport.run()'></div>");
    },
	run: function(){
        zyOffice.getInstance().api.exportWord();
	}
};
window.wangEditor.zyofficepdf = {
    init: function(editor,w) {    	
    	$(editor).find(" .w-e-toolbar").append("<div class='w-e-menu zyofficepdf' title='导入PDF文档' onclick='window.wangEditor.zyofficepdf.run()'></div>");
    },
	run: function(){
        zyOffice.getInstance().api.openPdf();
	}
};